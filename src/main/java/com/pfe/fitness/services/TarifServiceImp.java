package com.pfe.fitness.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pfe.fitness.entities.Tarif;
import com.pfe.fitness.repository.TarifRepository;

@Service
public class TarifServiceImp implements TarifService {
	
	 @Autowired
		private TarifRepository tarifRepository;
		@Override
		public List<Tarif> getAllTarif() {
			
			return tarifRepository.findAll();
		}

		@Override
		public Tarif findTarifById(Long id) {
			Optional<Tarif> taOptional = tarifRepository.findById(id);
			if (taOptional.isEmpty()) {
				return null;
			} else {
				return taOptional.get();
			}

		}

		@Override
		public Tarif createTarif(Tarif tarif) {
			return tarifRepository.save(tarif);
		}

		@Override
		public Tarif updateTarif(Tarif tarif) {
			Optional<Tarif> taOptional = tarifRepository.findById(tarif.getId());

			if (taOptional.isEmpty()) {
				return null;
			} else {
				return tarifRepository.save(tarif);
			}

		}

		@Override
		public void deleteTarif(long id) {

			tarifRepository.deleteById(id);
		}
		 public String deleteTarif(Long nom)
		 {	Tarif t=tarifRepository.findById(nom).orElse(null);
			 tarifRepository.delete(t);
			 return "Tarif que sa Nom "+t.getNom()+"est supprime";
		 }

		@Override
		public long save(Tarif ta) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public String deleteTarif(String nom) {
			// TODO Auto-generated method stub
			return null;
		}

}
