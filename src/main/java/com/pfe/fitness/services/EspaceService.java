package com.pfe.fitness.services;

import java.util.List;

import com.pfe.fitness.entities.Espace;
import com.pfe.fitness.entities.Events;


public interface EspaceService {
	public List<Espace> getAllEspace();
	public Espace findEspaceById(Long id);
	public Espace createEspace(Espace espace);
	public void  deleteEspace(Long id);
	public Espace updateEspace(Espace espace);



}
