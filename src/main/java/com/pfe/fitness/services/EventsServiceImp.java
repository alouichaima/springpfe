package com.pfe.fitness.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pfe.fitness.entities.Events;
import com.pfe.fitness.payload.response.MessageResponse;
import com.pfe.fitness.repository.EventsRepository;

@Service
public class EventsServiceImp implements EventsService {

    @Autowired
	private EventsRepository eventsRepository;
	@Override
	public List<Events> getAllEvents() {
		
		return eventsRepository.findAll();
	}

	@Override
	public Events findEventsById(Long id) {
		Optional<Events> evOptional = eventsRepository.findById(id);
		if (evOptional.isEmpty()) {
			return null;
		} else {
			return evOptional.get();
		}

	}

	@Override
	public Events createEvents(Events events) {
		return eventsRepository.save(events);
	}

	@Override
	public Events updateEvents(Events events) {
		Optional<Events> evOptional = eventsRepository.findById(events.getId());

		if (evOptional.isEmpty()) {
			return null;
		} else {
			return eventsRepository.save(events);
		}

	}

	@Override
	public void deleteEvents(long id) {

		eventsRepository.deleteById(id);
	}
	 public String deleteEtudiant(Long nomE)
	 {	Events e=eventsRepository.findById(nomE).orElse(null);
		 eventsRepository.delete(e);
		 return "Etudiant que sa CIN numero "+e.getNomE()+"est supprime";
	 }

	@Override
	public long save(Events ev) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String deleteEvents(String nomE) {
		// TODO Auto-generated method stub
		return null;
	}

/*	@Override
	public long save(Events events) {
		System.out.println("save Events");
		Events ev = new Events();
		events.setNomE(events.getNomE());
		events.setDateE(events.getDateE());
		events.setDescription(events.getDescription());
		events.setFileName(events.getFileName());
		return  eventsRepository.save(ev)
				           .getId();
		
	}*/

}
