package com.pfe.fitness.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pfe.fitness.entities.Activite;
import com.pfe.fitness.entities.Coach;
import com.pfe.fitness.repository.CoachRepository;

@Service
public class CoachServiceImp implements CoachService {
	
	@Autowired
	private CoachRepository coachRepository;

	
	@Override
	public List<Coach> getAllCoach() {
		return coachRepository.findAll();
	}
	
	@Override
	public Coach findCoachById(Long id) {
		Optional<Coach> coaOptional = coachRepository.findById(id);
		if (coaOptional.isEmpty()) {
			return null;
		} else {
			return coaOptional.get();
		}
	}

	@Override
	public Coach createCoach(Coach coach) {
		return coachRepository.save(coach);

	}

	@Override
	public Coach updateCoach(Coach coach) {
		Optional<Coach> actOptional = coachRepository.findById(coach.getId());

		if (actOptional.isEmpty()) {
			return null;
		} else {
			return coachRepository.save(coach);
		}

	}

	@Override
	public void deleteCoach(long id) {
		coachRepository.deleteById(id);
		
	}

	@Override
	public String deleteCoach(String nomC) {
		// TODO Auto-generated method stub
		return null;
	}



}
