package com.pfe.fitness.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.fitness.entities.Activite;
import com.pfe.fitness.entities.Espace;
import com.pfe.fitness.entities.Events;
import com.pfe.fitness.services.EspaceService;

@RestController
@RequestMapping("/espace")
public class EspaceController {
	
	@Autowired
	private EspaceService espaceService ;

	@GetMapping(path="/all")
	public List<Espace> getAllEspace() {
		return espaceService.getAllEspace();
		
	}
	@GetMapping("/{id}")
	public ResponseEntity<Espace>  findEspaceById (@PathVariable Long id) {
		Espace espace = espaceService.findEspaceById(id);
		if (espace==null) {
			return new ResponseEntity<Espace>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<Espace>(espace, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public Espace createEspace(@RequestBody Espace espace) {
		return espaceService.createEspace(espace);
	}
	
	@PutMapping
	public Espace updateEspace(@RequestBody Espace espace) {
		return espaceService.updateEspace(espace);
				
	}
	@DeleteMapping(path= "/{id}") 
	public void deleteEspace(@PathVariable Long id) {
		 espaceService.deleteEspace(id);
	}
}
