package com.pfe.fitness.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.fitness.entities.Coach;
import com.pfe.fitness.services.CoachService;

@RequestMapping("/coach")
@RestController
public class CoachController {
	
	@Autowired
	private CoachService coachService;
	
	@GetMapping(path="/all")
	public List<Coach> getAllCoach() {
		return coachService.getAllCoach();
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Coach>  findCoachById (@PathVariable Long id) {
		Coach coach = coachService.findCoachById(id);
		if (coach==null) {
			return new ResponseEntity<Coach>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<Coach>(coach, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public Coach createCoach(@RequestBody Coach coach) {
		return coachService.createCoach(coach);
	}
	
	@PutMapping
	public Coach updateCoach(@RequestBody Coach coach) {
		return coachService.updateCoach(coach);
				
	}
	@DeleteMapping(path= "/{id}") 
	public void deleteCoach(@PathVariable Long id) {
		 coachService.deleteCoach(id);
	}
	


}
